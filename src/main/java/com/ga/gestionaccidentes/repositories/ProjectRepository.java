package com.ga.gestionaccidentes.repositories;

import com.ga.gestionaccidentes.model.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, Long>{
}
