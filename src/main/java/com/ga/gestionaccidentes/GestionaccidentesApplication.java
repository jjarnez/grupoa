package com.ga.gestionaccidentes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionaccidentesApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestionaccidentesApplication.class, args);
    }
}
